(ns ui.patterns)

;; patterns
(def pink-dots { :id "pink-circles-2"
                 :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMS41JyBjeT0nMS41JyByPScxLjUnIGZpbGw9JyNmZjAwNDgnLz4KPC9zdmc+Cg==" })

(def pink-dots-1 { :id "pink-circles-1"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCkiIC8+CiAgPGNpcmNsZSBjeD0iMSIgY3k9IjEiIHI9IjEiIGZpbGw9IiNmYjVkNjciLz4KPC9zdmc+" })

(def pink-dots-2 { :id "pink-circles-3"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMicgY3k9JzInIHI9JzInIGZpbGw9JyNmYjVkNjcnLz4KPC9zdmc+" })

(def pink-dots-3 { :id "pink-circles-5"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMycgY3k9JzMnIHI9JzMnIGZpbGw9JyNmYjVkNjcnLz4KPC9zdmc+Cg==" })

(def pink-dots-4 { :id "pink-circles-7"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNCcgY3k9JzQnIHI9JzQnIGZpbGw9JyNmYjVkNjcnLz4KPC9zdmc+" })

(def pink-dots-5 { :id "pink-circles-9"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNC41JyBjeT0nNC41JyByPSc0LjUnIGZpbGw9JyNmYjVkNjcnLz4KPC9zdmc+" })

(def pink-lines { :id "pink-stripe-3"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzMnIGZpbGw9JyNmZjAwNDgnIC8+Cjwvc3ZnPg=="})
(def pink-lines-1 {:id "pink-stripe-1"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PScxJyBmaWxsPScjZmI1ZDY3JyAvPgo8L3N2Zz4="})
(def pink-lines-2 {:id "pink-stripe-2"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PScyJyBmaWxsPScjZmI1ZDY3JyAvPgo8L3N2Zz4="})
(def pink-lines-3 {:id "pink-stripe-4"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc0JyBmaWxsPScjZmI1ZDY3JyAvPgo8L3N2Zz4="})
(def pink-lines-4 {:id "pink-stripe-6"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc2JyBmaWxsPScjZmI1ZDY3JyAvPgo8L3N2Zz4="})
(def pink-lines-5 {:id "pink-stripe-8"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc4JyBmaWxsPScjZmI1ZDY3JyAvPgo8L3N2Zz4="})

(def blue-dots { :id "blue-circles-4"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMi41JyBjeT0nMi41JyByPScyLjUnIGZpbGw9JyMwMDU0YTgnLz4KPC9zdmc+"})
                  
(def blue-dots-1 {:id "blue-circles-1"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJyZ2JhKDAsMCwwLDApIiAvPgogIDxjaXJjbGUgY3g9IjEiIGN5PSIxIiByPSIxIiBmaWxsPSIjMDAxNzUyIi8+Cjwvc3ZnPg=="}) 
(def blue-dots-2 {:id "blue-circles-3"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzInIGN5PScyJyByPScyJyBmaWxsPScjMDAxNzUyJy8+Cjwvc3ZnPg=="}) 
(def blue-dots-3 {:id "blue-circles-5"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzMnIGN5PSczJyByPSczJyBmaWxsPScjMDAxNzUyJy8+Cjwvc3ZnPgo="}) 
(def blue-dots-4 {:id "blue-circles-7"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzQnIGN5PSc0JyByPSc0JyBmaWxsPScjMDAxNzUyJy8+Cjwvc3ZnPg=="}) 
(def blue-dots-5 {:id "blue-circles-9"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzUnIGN5PSc1JyByPSc1JyBmaWxsPScjMDAxNzUyJy8+Cjwvc3ZnPg=="}) 

(def blue-lines { :id "blue-stripe-4"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzQnIGZpbGw9JyMwMDU0YTgnIC8+Cjwvc3ZnPg==" })
(def blue-lines-1 {:id "blue-stripe-1"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxJyBoZWlnaHQ9JzEwJyBmaWxsPScjMDAxNzUyJyAvPgo8L3N2Zz4="})
(def blue-lines-2 {:id "blue-stripe-3"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSczJyBoZWlnaHQ9JzEwJyBmaWxsPScjMDAxNzUyJyAvPgo8L3N2Zz4="})
(def blue-lines-3 {:id "blue-stripe-5"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc1JyBoZWlnaHQ9JzEwJyBmaWxsPScjMDAxNzUyJyAvPgo8L3N2Zz4="})
(def blue-lines-4 {:id "blue-stripe-7"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc3JyBoZWlnaHQ9JzEwJyBmaWxsPScjMDAxNzUyJyAvPgo8L3N2Zz4="})
(def blue-lines-5 {:id "blue-stripe-9"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc5JyBoZWlnaHQ9JzEwJyBmaWxsPScjMDAxNzUyJyAvPgo8L3N2Zz4="})

(def navy-dots { :id "navy-circles-4"
                 :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDI1NSwgMjU1LCAyNTUsIDApJyAvPgogIDxjaXJjbGUgY3g9JzIuNScgY3k9JzIuNScgcj0nMi41JyBmaWxsPScjMDAxNzUyJy8+Cjwvc3ZnPg=="})

(def navy-dots-1 {:id "navy-circles-1" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJyZ2JhKDAsMCwwLDApIiAvPgogIDxjaXJjbGUgY3g9IjEiIGN5PSIxIiByPSIxIiBmaWxsPSIjMTYxZjkzIi8+Cjwvc3ZnPg==" })
(def navy-dots-2 {:id "navy-circles-3" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzInIGN5PScyJyByPScyJyBmaWxsPScjMTYxZjkzJy8+Cjwvc3ZnPg==" })
(def navy-dots-3 {:id "navy-circles-5" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzMnIGN5PSczJyByPSczJyBmaWxsPScjMTYxZjkzJy8+Cjwvc3ZnPgo=" })
(def navy-dots-4 {:id "navy-circles-7" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzQnIGN5PSc0JyByPSc0JyBmaWxsPScjMTYxZjkzJy8+Cjwvc3ZnPg==" })
(def navy-dots-5 {:id "navy-circles-9" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzUnIGN5PSc1JyByPSc1JyBmaWxsPScjMTYxZjkzJy8+Cjwvc3ZnPg==" })
                  
(def navy-lines { :id "navy-stripe-3"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDI1NSwgMjU1LCAyNTUsIDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSczJyBmaWxsPScjMDAxNzUyJyAvPgo8L3N2Zz4=" })

(def navy-lines-1 {:id "navy-stripe-1" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PScxJyBmaWxsPScjMTYxZjkzJyAvPgo8L3N2Zz4="})
(def navy-lines-2 {:id "navy-stripe-2" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PScyJyBmaWxsPScjMTYxZjkzJyAvPgo8L3N2Zz4="})
(def navy-lines-3 {:id "navy-stripe-4" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc0JyBmaWxsPScjMTYxZjkzJyAvPgo8L3N2Zz4="})
(def navy-lines-4 {:id "navy-stripe-6" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc2JyBmaWxsPScjMTYxZjkzJyAvPgo8L3N2Zz4="})
(def navy-lines-5 {:id "navy-stripe-8" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc4JyBmaWxsPScjMTYxZjkzJyAvPgo8L3N2Zz4="})

(def yellow-dots { :id "yellow-circles-3"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMicgY3k9JzInIHI9JzInIGZpbGw9JyNmZmY3MGYnLz4KPC9zdmc+" })

(def yellow-dots-1 {:id "yellow-circles-1"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJyZ2JhKDAsMCwwLDApIiAvPgogIDxjaXJjbGUgY3g9IjEiIGN5PSIxIiByPSIxIiBmaWxsPSIjZmZmNzBmIi8+Cjwvc3ZnPg=="})
                    
(def yellow-dots-2 {:id "yellow-circles-2"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzEuNScgY3k9JzEuNScgcj0nMS41JyBmaWxsPScjZmZmNzBmJy8+Cjwvc3ZnPgo="})
                    
(def yellow-dots-3 {:id "yellow-circles-4"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzIuNScgY3k9JzIuNScgcj0nMi41JyBmaWxsPScjZmZmNzBmJy8+Cjwvc3ZnPg=="})
                    
(def yellow-dots-4 {:id "yellow-circles-6"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzMuNScgY3k9JzMuNScgcj0nMy41JyBmaWxsPScjZmZmNzBmJy8+Cjwvc3ZnPgo="})
                    
(def yellow-dots-5 {:id "yellow-circles-8"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzQuNScgY3k9JzQuNScgcj0nNC41JyBmaWxsPScjZmZmNzBmJy8+Cjwvc3ZnPg=="})
                    
(def yellow-lines { :id "yellow-stripe-3"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzMnIGZpbGw9JyNmZmY3MGYnIC8+Cjwvc3ZnPg==" })
                    
(def yellow-lines-1 {:id "yellow-stripe-1"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxJyBoZWlnaHQ9JzEwJyBmaWxsPScjZmZmNzBmJyAvPgo8L3N2Zz4="})
                    
(def yellow-lines-2 {:id "yellow-stripe-2"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScyJyBoZWlnaHQ9JzEwJyBmaWxsPScjZmZmNzBmJyAvPgo8L3N2Zz4="})
                    
(def yellow-lines-3 {:id "yellow-stripe-4"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc0JyBoZWlnaHQ9JzEwJyBmaWxsPScjZmZmNzBmJyAvPgo8L3N2Zz4="})
                    
(def yellow-lines-4 {:id "yellow-stripe-6"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc2JyBoZWlnaHQ9JzEwJyBmaWxsPScjZmZmNzBmJyAvPgo8L3N2Zz4="})
                    
(def yellow-lines-5 {:id "yellow-stripe-8"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc4JyBoZWlnaHQ9JzEwJyBmaWxsPScjZmZmNzBmJyAvPgo8L3N2Zz4="})
                    
(def gray-dots { :id "gray-circles-3"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMicgY3k9JzInIHI9JzInIGZpbGw9JyM0YTRmNTQnLz4KPC9zdmc+"})

(def gray-dots-lg { :id "gray-circles-7"
                     :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNCcgY3k9JzQnIHI9JzQnIGZpbGw9JyM0YTRmNTQnLz4KPC9zdmc+"})
                     
(def gray-dots-1 {:id "gray-circles-2"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzEuNScgY3k9JzEuNScgcj0nMS41JyBmaWxsPScjNTM1MzVjJy8+Cjwvc3ZnPgo="})

(def gray-dots-2 {:id "gray-circles-4"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzIuNScgY3k9JzIuNScgcj0nMi41JyBmaWxsPScjNTM1MzVjJy8+Cjwvc3ZnPg=="})

(def gray-dots-3 {:id "gray-circles-6"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzMuNScgY3k9JzMuNScgcj0nMy41JyBmaWxsPScjNTM1MzVjJy8+Cjwvc3ZnPgo="})
                     
(def gray-dots-4 gray-dots-lg)
(def gray-dots-5 {:id "gray-circles-9"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzUnIGN5PSc1JyByPSc1JyBmaWxsPScjNTM1MzVjJy8+Cjwvc3ZnPg=="})

(def gray-lines { :id "gray-stripe-4"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzQnIGZpbGw9JyM0YTRmNTQnIC8+Cjwvc3ZnPg=="})
(def gray-lines-1 { :id "gray-stripe-1"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxJyBoZWlnaHQ9JzEwJyBmaWxsPScjNTM1MzVjJyAvPgo8L3N2Zz4="})
(def gray-lines-2 { :id "gray-stripe-3"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSczJyBoZWlnaHQ9JzEwJyBmaWxsPScjNTM1MzVjJyAvPgo8L3N2Zz4="})
(def gray-lines-3 { :id "gray-stripe-5"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc1JyBoZWlnaHQ9JzEwJyBmaWxsPScjNTM1MzVjJyAvPgo8L3N2Zz4="})
(def gray-lines-4 { :id "gray-stripe-7"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc3JyBoZWlnaHQ9JzEwJyBmaWxsPScjNTM1MzVjJyAvPgo8L3N2Zz4="})
(def gray-lines-5 { :id "gray-stripe-9"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPSc5JyBoZWlnaHQ9JzEwJyBmaWxsPScjNTM1MzVjJyAvPgo8L3N2Zz4="})

(def gray-patch { :id "gary-subtle-patch"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc1JyBoZWlnaHQ9JzUnPgogIDxyZWN0IHdpZHRoPSc1JyBoZWlnaHQ9JzUnIGZpbGw9J2hzbGEoMzYwLCAxMDAlLCAxMDAlLCAwKScgLz4KICA8cmVjdCB4PScyJyB5PScyJyB3aWR0aD0nMScgaGVpZ2h0PScxJyBmaWxsPScjNGE0ZjU0JyAvPgo8L3N2Zz4="})

(def shadow { :id "gray-circles-9"
              :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNScgY3k9JzUnIHI9JzUnIGZpbGw9JyM0YTRmNTQnLz4KPC9zdmc+"})

(def white-dots { :id "white-circles-3"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMicgY3k9JzInIHI9JzInIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScvPgo8L3N2Zz4=" })

(def white-dots-lg { :id "white-circles-9"
                     :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNScgY3k9JzUnIHI9JzUnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScvPgo8L3N2Zz4=" })
                     
(def white-dots-1 {:id "white-circles-1"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJoc2xhKDI0MCwgNiUsIDkwJSwgMCkiIC8+CiAgPGNpcmNsZSBjeD0iMSIgY3k9IjEiIHI9IjEiIGZpbGw9ImhzbGEoMjQwLCA2JSwgOTAlLCAxKSIvPgo8L3N2Zz4="})
(def white-dots-2 {:id "white-circles-2"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMS41JyBjeT0nMS41JyByPScxLjUnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScvPgo8L3N2Zz4K"})
(def white-dots-3 {:id "white-circles-4"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMi41JyBjeT0nMi41JyByPScyLjUnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScvPgo8L3N2Zz4="})
(def white-dots-4 {:id "white-circles-6"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMy41JyBjeT0nMy41JyByPSczLjUnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScvPgo8L3N2Zz4K"})
(def white-dots-5 {:id "white-circles-8"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNC41JyBjeT0nNC41JyByPSc0LjUnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScvPgo8L3N2Zz4="})

(def white-lines { :id "white-stripe-4"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzQnIGhlaWdodD0nMTAnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScgLz4KPC9zdmc+" })
                   
(def white-lines-1 {:id "white-stripe-1"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzEnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScgLz4KPC9zdmc+"})
(def white-lines-2 {:id "white-stripe-3"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzMnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScgLz4KPC9zdmc+"})
(def white-lines-3 {:id "white-stripe-5"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzUnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScgLz4KPC9zdmc+"})
(def white-lines-4 {:id "white-stripe-7"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzcnIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScgLz4KPC9zdmc+"})
(def white-lines-5 {:id "white-stripe-9"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDI0MCwgNiUsIDkwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzknIGZpbGw9J2hzbGEoMjQwLCA2JSwgOTAlLCAxKScgLz4KPC9zdmc+"})

(def mint-dots { :id "mint-circles-4"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMi41JyBjeT0nMi41JyByPScyLjUnIGZpbGw9JyMwMGJhYTknLz4KPC9zdmc+" })
                  
(def mint-dots-1 {:id "mint-circles-1" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCkiIC8+CiAgPGNpcmNsZSBjeD0iMSIgY3k9IjEiIHI9IjEiIGZpbGw9IiMwMGJhYTkiLz4KPC9zdmc+"})
(def mint-dots-2 {:id "mint-circles-3" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMicgY3k9JzInIHI9JzInIGZpbGw9JyMwMGJhYTknLz4KPC9zdmc+"})
(def mint-dots-3 {:id "mint-circles-5" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMycgY3k9JzMnIHI9JzMnIGZpbGw9JyMwMGJhYTknLz4KPC9zdmc+Cg=="})
(def mint-dots-4 {:id "mint-circles-7" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNCcgY3k9JzQnIHI9JzQnIGZpbGw9JyMwMGJhYTknLz4KPC9zdmc+"})
(def mint-dots-5 {:id "mint-circles-9" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNScgY3k9JzUnIHI9JzUnIGZpbGw9JyMwMGJhYTknLz4KPC9zdmc+"})

(def mint-lines { :id "mint-stripe-3"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzMnIGZpbGw9JyMwMGJhYTknIC8+Cjwvc3ZnPg==" })
                   
 (def mint-lines-1 {:id "mint-stripe-1" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEnIGhlaWdodD0nMTAnIGZpbGw9JyMwMGJhYTknIC8+Cjwvc3ZnPg=="})
 
 (def mint-lines-2 {:id "mint-stripe-2" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzInIGhlaWdodD0nMTAnIGZpbGw9JyMwMGJhYTknIC8+Cjwvc3ZnPg=="})
 
 (def mint-lines-3 {:id "mint-stripe-4" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzQnIGhlaWdodD0nMTAnIGZpbGw9JyMwMGJhYTknIC8+Cjwvc3ZnPg=="})
 
 (def mint-lines-4 {:id "mint-stripe-6" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzYnIGhlaWdodD0nMTAnIGZpbGw9JyMwMGJhYTknIC8+Cjwvc3ZnPg=="})
 
 (def mint-lines-5 {:id "mint-stripe-8" 
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzgnIGhlaWdodD0nMTAnIGZpbGw9JyMwMGJhYTknIC8+Cjwvc3ZnPg=="})

(def orange-dots {:id "orange-circles-3"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMicgY3k9JzInIHI9JzInIGZpbGw9JyNmZmFhMzcnLz4KPC9zdmc+" })
                  
(def orange-dots-1 {:id "orange-circles-1"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCkiIC8+CiAgPGNpcmNsZSBjeD0iMSIgY3k9IjEiIHI9IjEiIGZpbGw9IiNmZmFhMzciLz4KPC9zdmc+"})
                    
(def orange-dots-2 {:id "orange-circles-2"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMS41JyBjeT0nMS41JyByPScxLjUnIGZpbGw9JyNmZmFhMzcnLz4KPC9zdmc+Cg=="})
                    
(def orange-dots-3 {:id "orange-circles-4"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMi41JyBjeT0nMi41JyByPScyLjUnIGZpbGw9JyNmZmFhMzcnLz4KPC9zdmc+"})
                    
(def orange-dots-4 {:id "orange-circles-6"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMy41JyBjeT0nMy41JyByPSczLjUnIGZpbGw9JyNmZmFhMzcnLz4KPC9zdmc+Cg=="})
                    
(def orange-dots-5 {:id "orange-circles-8"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNC41JyBjeT0nNC41JyByPSc0LjUnIGZpbGw9JyNmZmFhMzcnLz4KPC9zdmc+"})
                    


(def orange-lines { :id "orange-stripe-3"
                   :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzMnIGZpbGw9JyNmZmFhMzcnIC8+Cjwvc3ZnPg==" })
                   
(def orange-lines-1 {:id "orange-stripe-1"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzEnIGZpbGw9JyNmZmFhMzcnIC8+Cjwvc3ZnPg=="})
                    
(def orange-lines-2 {:id "orange-stripe-2"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzInIGZpbGw9JyNmZmFhMzcnIC8+Cjwvc3ZnPg=="})
                    
(def orange-lines-3 {:id "orange-stripe-4"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzQnIGZpbGw9JyNmZmFhMzcnIC8+Cjwvc3ZnPg=="})
                    
(def orange-lines-4 {:id "orange-stripe-6"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzYnIGZpbGw9JyNmZmFhMzcnIC8+Cjwvc3ZnPg=="})
                    
(def orange-lines-5 {:id "orange-stripe-8"
                    :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEwJyBoZWlnaHQ9JzgnIGZpbGw9JyNmZmFhMzcnIC8+Cjwvc3ZnPg=="})
                    

(def br-orange-dots { :id "br-orange-circles-3"
                 :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMicgY3k9JzInIHI9JzInIGZpbGw9JyNmODAnLz4KPC9zdmc+" })


(def br-orange-dots-1 {:id "br-orange-circles-1"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCkiIC8+CiAgPGNpcmNsZSBjeD0iMSIgY3k9IjEiIHI9IjEiIGZpbGw9IiNmODAiLz4KPC9zdmc+"})

(def br-orange-dots-2 {:id "br-orange-circles-2"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMS41JyBjeT0nMS41JyByPScxLjUnIGZpbGw9JyNmODAnLz4KPC9zdmc+Cg=="})

(def br-orange-dots-3 {:id "br-orange-circles-4"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMi41JyBjeT0nMi41JyByPScyLjUnIGZpbGw9JyNmODAnLz4KPC9zdmc+"})

(def br-orange-dots-4 {:id "br-orange-circles-6"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nMy41JyBjeT0nMy41JyByPSczLjUnIGZpbGw9JyNmODAnLz4KPC9zdmc+Cg=="})

(def br-orange-dots-5 {:id "br-orange-circles-8"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPGNpcmNsZSBjeD0nNC41JyBjeT0nNC41JyByPSc0LjUnIGZpbGw9JyNmODAnLz4KPC9zdmc+"})

(def br-orange-lines { :id "br-orange-stripe-3"
                  :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzMnIGhlaWdodD0nMTAnIGZpbGw9JyNmODAnIC8+Cjwvc3ZnPg==" })

(def br-orange-lines-1 {:id "br-orange-stripe-1"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzEnIGhlaWdodD0nMTAnIGZpbGw9JyNmODAnIC8+Cjwvc3ZnPg=="})
                       
(def br-orange-lines-2 {:id "br-orange-stripe-2"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzInIGhlaWdodD0nMTAnIGZpbGw9JyNmODAnIC8+Cjwvc3ZnPg=="})
                       
(def br-orange-lines-3 {:id "br-orange-stripe-4"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzQnIGhlaWdodD0nMTAnIGZpbGw9JyNmODAnIC8+Cjwvc3ZnPg=="})
                       
(def br-orange-lines-4 {:id "br-orange-stripe-6"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzYnIGhlaWdodD0nMTAnIGZpbGw9JyNmODAnIC8+Cjwvc3ZnPg=="})
                       
(def br-orange-lines-5 {:id "br-orange-stripe-8"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdoc2xhKDM2MCwgMTAwJSwgMTAwJSwgMCknIC8+CiAgPHJlY3QgeD0nMCcgeT0nMCcgd2lkdGg9JzgnIGhlaWdodD0nMTAnIGZpbGw9JyNmODAnIC8+Cjwvc3ZnPg=="})

(def midnight-dots-1 {:id "midnight-circles-1"
                      :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSJyZ2JhKDAsMCwwLDApIiAvPgogIDxjaXJjbGUgY3g9IjEiIGN5PSIxIiByPSIxIiBmaWxsPSIjMDUwNjFmIi8+Cjwvc3ZnPg==" })
                      
(def midnight-dots-2 {:id "midnight-circles-3"
                      :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzInIGN5PScyJyByPScyJyBmaWxsPScjMDUwNjFmJy8+Cjwvc3ZnPg==" })
                      
(def midnight-dots-3 {:id "midnight-circles-5"
                      :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzMnIGN5PSczJyByPSczJyBmaWxsPScjMDUwNjFmJy8+Cjwvc3ZnPgo=" })
                      
(def midnight-dots-4 {:id "midnight-circles-7"
                      :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzQnIGN5PSc0JyByPSc0JyBmaWxsPScjMDUwNjFmJy8+Cjwvc3ZnPg==" })
                      
(def midnight-dots-5 {:id "midnight-circles-9"
                      :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxjaXJjbGUgY3g9JzUnIGN5PSc1JyByPSc1JyBmaWxsPScjMDUwNjFmJy8+Cjwvc3ZnPg==" })
                      
(def midnight-lines-1 {:id "midnight-stripe-1"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PScxJyBmaWxsPScjMDUwNjFmJyAvPgo8L3N2Zz4="})
                      
(def midnight-lines-2 {:id "midnight-stripe-3"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSczJyBmaWxsPScjMDUwNjFmJyAvPgo8L3N2Zz4="})
                      
(def midnight-lines-3 {:id "midnight-stripe-5"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc1JyBmaWxsPScjMDUwNjFmJyAvPgo8L3N2Zz4="})
                      
(def midnight-lines-4 {:id "midnight-stripe-7"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc3JyBmaWxsPScjMDUwNjFmJyAvPgo8L3N2Zz4="})
                      
(def midnight-lines-5 {:id "midnight-stripe-9"
                       :image-link "data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPScxMCcgaGVpZ2h0PScxMCc+CiAgPHJlY3Qgd2lkdGg9JzEwJyBoZWlnaHQ9JzEwJyBmaWxsPSdyZ2JhKDAsMCwwLDApJyAvPgogIDxyZWN0IHg9JzAnIHk9JzAnIHdpZHRoPScxMCcgaGVpZ2h0PSc5JyBmaWxsPScjMDUwNjFmJyAvPgo8L3N2Zz4="})                        
                    

;; mindnight 

(def all-patterns 
  [blue-dots blue-dots-1 blue-dots-2 blue-dots-3 blue-dots-4 blue-dots-5
   blue-lines blue-lines-1 blue-lines-2 blue-lines-3 blue-lines-4 blue-lines-5
   pink-dots pink-dots-1 pink-dots-2 pink-dots-3 pink-dots-4 pink-dots-5
   pink-lines pink-lines-1 pink-lines-2 pink-lines-3 pink-lines-4 pink-lines-5
   gray-dots gray-dots-lg gray-dots-1 gray-dots-2 gray-dots-3 gray-dots-4 gray-dots-5
   gray-lines gray-lines-1 gray-lines-2 gray-lines-3 gray-lines-4 gray-lines-5
   gray-patch
   mint-dots mint-dots-1 mint-dots-2 mint-dots-3 mint-dots-4 mint-dots-5
   mint-lines mint-lines-1 mint-lines-2 mint-lines-3 mint-lines-4 mint-lines-5
   navy-dots navy-dots-1 navy-dots-2 navy-dots-3 navy-dots-4 navy-dots-5
   navy-lines navy-lines-1 navy-lines-2 navy-lines-3 navy-lines-4 navy-lines-5
   orange-dots orange-dots-1 orange-dots-2 orange-dots-3 orange-dots-4 orange-dots-5 orange-lines orange-lines-1 orange-lines-2 orange-lines-3 orange-lines-4 orange-lines-5
   br-orange-dots br-orange-dots-1 br-orange-dots-2 br-orange-dots-3 br-orange-dots-4 br-orange-dots-5  br-orange-lines br-orange-lines-1 br-orange-lines-2 br-orange-lines-3 br-orange-lines-4 br-orange-lines-5
   yellow-dots yellow-dots-1 yellow-dots-2 yellow-dots-3 yellow-dots-4 yellow-dots-5
   yellow-lines yellow-lines-1 yellow-lines-2 yellow-lines-3 yellow-lines-4 yellow-lines-5
   midnight-dots-1 midnight-dots-2 midnight-dots-3 midnight-dots-4 midnight-dots-5  midnight-lines-1 midnight-lines-2 midnight-lines-3 midnight-lines-4 midnight-lines-5
   white-dots white-dots-1 white-dots-2 white-dots-3 white-dots-4 white-dots-5
   white-dots-lg
   white-lines white-lines-1 white-lines-2 white-lines-3 white-lines-4 white-lines-5
   shadow])

;; gen pattern string to use patterns as colors
(defn pattern
  [ fill-id ]
  (str "url(#" fill-id ") #fff"))

;; pattern gen fn for defs on init
(defn pattern-def
  [{ :keys [id image-link] }]
  [:pattern { :id id
              :patternUnits "userSpaceOnUse"
              :width "10"
              :height "10"
              :key (random-uuid)}
    [:image { :xlinkHref image-link
              :x "0"
              :y "0"
              :width "10"
              :height "10"
              :key (random-uuid) }]])

;; noise pattern for now — TODO: replace me

(def noise
  [:pattern { :id "noise"
              :patternUnits "userSpaceOnUse"
              :width "100"
              :height "100"
              :key (random-uuid)}
    [:g {
      :fill "none"
      :stroke "#fff"
      :stroke-width "2"
      :opacity ".5"}

      [:polyline {
        :stroke-dasharray "1 3 1 4 1 5 1 4 1 8 1 3 1 6 1 6 1 3 1 5 1 4 1 3 1 5 1 3 1 5 1 3 1 4 1 5 1 4 1 3 1 4 1 3 1 5 1 3 1 5 1 4 1 4 1 5 1 3 1 4 1 6 1 4 1 6 1 3 1 6 1 4 1 3 1 5 1 4 1 6 1 5 1 4 1 5 1 4 1 4 1 6 1 3 1 5 1 4 1 6 1 4 1 4 1 6 1 5 1 3 1 4 1 4 1 3 1 6 1 6 1 6 1 5 1 5 1 4 1 4 1 4 1 4 1 4 1 6 1 3 1 6 1 4 1 5 1 6 1 6 1 5 1 3 1 5 1 3 1 5 1 3 1 4 1 5 1 6 1 3 1 6 1 4 1 3 1 6 1 4 1 6 1 5 1 5 1 3 1 3 1 3 1 5 1 4 1 5 1 4 1 3 1 4 1 5 1 6 1 4 1 6 1 4 1 4 1 4 1 4 1 3 1 6 1 3 1 4 1 3 1 3 1 6 1 5 1 4 1 5 1 4 1 4 1 4 1 6 1 6 1 5 1 5 1 5 1 6 1 6 1 6 1 6 1 6 1 3 1 4 1 4 1 3 1 6 1 6 1 4 1 3 1 6 1 6 1 3 1 6 1 3 1 5 1 3 1 3 1 5 1 4 1 4 1 3 1 6 1 3 1 3 1 4 1 5 1 6 1 3 1 6 1 3 1 5 1 3 1 5 1 5 1 3 1 4 1 5 1 6 1 4 1 6 1 3 1 4 1 6 1 3 1 3 1 3 1 4 1 5 1 3 1 4 1 4 1 5 1 5 1 5 1 4 1 5 1 5 1 5 1 5 1 5 1 6 1 4 1 6 1 5 1 4"
        :points "0.5,0
          0.5,99.5 1.5,99.5 1.5,0.5 2.5,0.5 2.5,99.5 3.5,99.5 3.5,0.5 4.5,0.5 4.5,99.5 5.5,99.5 5.5,0.5 6.5,0.5 6.5,99.5 7.5,99.5
          7.5,0.5 8.5,0.5 8.5,99.5 9.5,99.5 9.5,0.5 10.5,0.5 10.5,99.5 11.5,99.5 11.5,0.5 12.5,0.5 12.5,99.5 13.5,99.5 13.5,0.5 14.5,0.5
          14.5,99.5 15.5,99.5 15.5,0.5 16.5,0.5 16.5,99.5 17.5,99.5 17.5,0.5 18.5,0.5 18.5,99.5 19.5,99.5 19.5,0.5 20.5,0.5 20.5,99.5
          21.5,99.5 21.5,0.5 22.5,0.5 22.5,99.5 23.5,99.5 23.5,0.5 24.5,0.5 24.5,99.5 25.5,99.5 25.5,0.5 26.5,0.5 26.5,99.5 27.5,99.5
          27.5,0.5 28.5,0.5 28.5,99.5 29.5,99.5 29.5,0.5 30.5,0.5 30.5,99.5 31.5,99.5 31.5,0.5 32.5,0.5 32.5,99.5 33.5,99.5 33.5,0.5
          34.5,0.5 34.5,99.5 35.5,99.5 35.5,0.5 36.5,0.5 36.5,99.5 37.5,99.5 37.5,0.5 38.5,0.5 38.5,99.5 39.5,99.5 39.5,0.5 40.5,0.5
          40.5,99.5 41.5,99.5 41.5,0.5 42.5,0.5 42.5,99.5 43.5,99.5 43.5,0.5 44.5,0.5 44.5,99.5 45.5,99.5 45.5,0.5 46.5,0.5 46.5,99.5
          47.5,99.5 47.5,0.5 48.5,0.5 48.5,99.5 49.5,99.5 49.5,0.5 50.5,0.5 50.5,99.5 51.5,99.5 51.5,0.5 52.5,0.5 52.5,99.5 53.5,99.5
          53.5,0.5 54.5,0.5 54.5,99.5 55.5,99.5 55.5,0.5 56.5,0.5 56.5,99.5 57.5,99.5 57.5,0.5 58.5,0.5 58.5,99.5 59.5,99.5 59.5,0.5
          60.5,0.5 60.5,99.5 61.5,99.5 61.5,0.5 62.5,0.5 62.5,99.5 63.5,99.5 63.5,0.5 64.5,0.5 64.5,99.5 65.5,99.5 65.5,0.5 66.5,0.5
          66.5,99.5 67.5,99.5 67.5,0.5 68.5,0.5 68.5,99.5 69.5,99.5 69.5,0.5 70.5,0.5 70.5,99.5 71.5,99.5 71.5,0.5 72.5,0.5 72.5,99.5
          73.5,99.5 73.5,0.5 74.5,0.5 74.5,99.5 75.5,99.5 75.5,0.5 76.5,0.5 76.5,99.5 77.5,99.5 77.5,0.5 78.5,0.5 78.5,99.5 79.5,99.5
          79.5,0.5 80.5,0.5 80.5,99.5 81.5,99.5 81.5,0.5 82.5,0.5 82.5,99.5 83.5,99.5 83.5,0.5 84.5,0.5 84.5,99.5 85.5,99.5 85.5,0.5
          86.5,0.5 86.5,99.5 87.5,99.5 87.5,0.5 88.5,0.5 88.5,99.5 89.5,99.5 89.5,0.5 90.5,0.5 90.5,99.5 91.5,99.5 91.5,0.5 92.5,0.5
          92.5,99.5 93.5,99.5 93.5,0.5 94.5,0.5 94.5,99.5 95.5,99.5 95.5,0.5 96.5,0.5 96.5,99.5 97.5,99.5 97.5,0.5 98.5,0.5 98.5,99.5
          99.5,99.5 99.5,0.5"}]]])


(defn gen-color-noise
  [color]
  [:pattern { :id (str "noise-" color)
              :patternUnits "userSpaceOnUse"
              :width "100"
              :height "100"
              :key (random-uuid)}
    [:g {
      :fill "none"
      :stroke color
      :stroke-width "2"
      :opacity ".5"}

      [:polyline {
        :stroke-dasharray "1 3 1 4 1 5 1 4 1 8 1 3 1 6 1 6 1 3 1 5 1 4 1 3 1 5 1 3 1 5 1 3 1 4 1 5 1 4 1 3 1 4 1 3 1 5 1 3 1 5 1 4 1 4 1 5 1 3 1 4 1 6 1 4 1 6 1 3 1 6 1 4 1 3 1 5 1 4 1 6 1 5 1 4 1 5 1 4 1 4 1 6 1 3 1 5 1 4 1 6 1 4 1 4 1 6 1 5 1 3 1 4 1 4 1 3 1 6 1 6 1 6 1 5 1 5 1 4 1 4 1 4 1 4 1 4 1 6 1 3 1 6 1 4 1 5 1 6 1 6 1 5 1 3 1 5 1 3 1 5 1 3 1 4 1 5 1 6 1 3 1 6 1 4 1 3 1 6 1 4 1 6 1 5 1 5 1 3 1 3 1 3 1 5 1 4 1 5 1 4 1 3 1 4 1 5 1 6 1 4 1 6 1 4 1 4 1 4 1 4 1 3 1 6 1 3 1 4 1 3 1 3 1 6 1 5 1 4 1 5 1 4 1 4 1 4 1 6 1 6 1 5 1 5 1 5 1 6 1 6 1 6 1 6 1 6 1 3 1 4 1 4 1 3 1 6 1 6 1 4 1 3 1 6 1 6 1 3 1 6 1 3 1 5 1 3 1 3 1 5 1 4 1 4 1 3 1 6 1 3 1 3 1 4 1 5 1 6 1 3 1 6 1 3 1 5 1 3 1 5 1 5 1 3 1 4 1 5 1 6 1 4 1 6 1 3 1 4 1 6 1 3 1 3 1 3 1 4 1 5 1 3 1 4 1 4 1 5 1 5 1 5 1 4 1 5 1 5 1 5 1 5 1 5 1 6 1 4 1 6 1 5 1 4"
        :points "0.5,0
          0.5,99.5 1.5,99.5 1.5,0.5 2.5,0.5 2.5,99.5 3.5,99.5 3.5,0.5 4.5,0.5 4.5,99.5 5.5,99.5 5.5,0.5 6.5,0.5 6.5,99.5 7.5,99.5
          7.5,0.5 8.5,0.5 8.5,99.5 9.5,99.5 9.5,0.5 10.5,0.5 10.5,99.5 11.5,99.5 11.5,0.5 12.5,0.5 12.5,99.5 13.5,99.5 13.5,0.5 14.5,0.5
          14.5,99.5 15.5,99.5 15.5,0.5 16.5,0.5 16.5,99.5 17.5,99.5 17.5,0.5 18.5,0.5 18.5,99.5 19.5,99.5 19.5,0.5 20.5,0.5 20.5,99.5
          21.5,99.5 21.5,0.5 22.5,0.5 22.5,99.5 23.5,99.5 23.5,0.5 24.5,0.5 24.5,99.5 25.5,99.5 25.5,0.5 26.5,0.5 26.5,99.5 27.5,99.5
          27.5,0.5 28.5,0.5 28.5,99.5 29.5,99.5 29.5,0.5 30.5,0.5 30.5,99.5 31.5,99.5 31.5,0.5 32.5,0.5 32.5,99.5 33.5,99.5 33.5,0.5
          34.5,0.5 34.5,99.5 35.5,99.5 35.5,0.5 36.5,0.5 36.5,99.5 37.5,99.5 37.5,0.5 38.5,0.5 38.5,99.5 39.5,99.5 39.5,0.5 40.5,0.5
          40.5,99.5 41.5,99.5 41.5,0.5 42.5,0.5 42.5,99.5 43.5,99.5 43.5,0.5 44.5,0.5 44.5,99.5 45.5,99.5 45.5,0.5 46.5,0.5 46.5,99.5
          47.5,99.5 47.5,0.5 48.5,0.5 48.5,99.5 49.5,99.5 49.5,0.5 50.5,0.5 50.5,99.5 51.5,99.5 51.5,0.5 52.5,0.5 52.5,99.5 53.5,99.5
          53.5,0.5 54.5,0.5 54.5,99.5 55.5,99.5 55.5,0.5 56.5,0.5 56.5,99.5 57.5,99.5 57.5,0.5 58.5,0.5 58.5,99.5 59.5,99.5 59.5,0.5
          60.5,0.5 60.5,99.5 61.5,99.5 61.5,0.5 62.5,0.5 62.5,99.5 63.5,99.5 63.5,0.5 64.5,0.5 64.5,99.5 65.5,99.5 65.5,0.5 66.5,0.5
          66.5,99.5 67.5,99.5 67.5,0.5 68.5,0.5 68.5,99.5 69.5,99.5 69.5,0.5 70.5,0.5 70.5,99.5 71.5,99.5 71.5,0.5 72.5,0.5 72.5,99.5
          73.5,99.5 73.5,0.5 74.5,0.5 74.5,99.5 75.5,99.5 75.5,0.5 76.5,0.5 76.5,99.5 77.5,99.5 77.5,0.5 78.5,0.5 78.5,99.5 79.5,99.5
          79.5,0.5 80.5,0.5 80.5,99.5 81.5,99.5 81.5,0.5 82.5,0.5 82.5,99.5 83.5,99.5 83.5,0.5 84.5,0.5 84.5,99.5 85.5,99.5 85.5,0.5
          86.5,0.5 86.5,99.5 87.5,99.5 87.5,0.5 88.5,0.5 88.5,99.5 89.5,99.5 89.5,0.5 90.5,0.5 90.5,99.5 91.5,99.5 91.5,0.5 92.5,0.5
          92.5,99.5 93.5,99.5 93.5,0.5 94.5,0.5 94.5,99.5 95.5,99.5 95.5,0.5 96.5,0.5 96.5,99.5 97.5,99.5 97.5,0.5 98.5,0.5 98.5,99.5
          99.5,99.5 99.5,0.5"}]]])
