# Scribble

This is the code for Scribble, a CCC entry.

## To Run

In the root directory:

```bash
npm install electron -g
npm install
lein cooper
electron .
```
